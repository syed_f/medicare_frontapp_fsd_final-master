export class Users {

    id: number;
    fname: string;
    lname: string;
    email: string;
    password: string;
    category: string;
    token: string;

}
